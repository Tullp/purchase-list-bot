
import os


def abs_path(filename: str) -> str:
	"""
	Finds the absolute path to file by the relative path from the project root.

	:param filename: path to file from the project root.
	:returns: the absolute path to the given file
	"""
	return os.path.join(project_root(), filename)


def project_root() -> str:
	return os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
