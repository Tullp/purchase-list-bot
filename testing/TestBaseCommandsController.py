import os
from unittest import TestCase

from BotCore import DataBase

import Config
from Config import help_message
from client.BaseCommandsController import BaseCommandsController
from services.user.UserDao import UserDao
from services.user.UserService import UserService
from testing.TeleBotSpy import TeleBotSpy, FakeMessage
from testing.TestUtils import abs_path


class TestBaseCommandsController(TestCase):

	test_chat_id = 12345
	test_user = {"id": test_chat_id, "username": "test_username"}

	def setUp(self):
		self.bot = TeleBotSpy()
		self.db = DataBase("testing.sqlite", abs_path(Config.structure_path))
		self.controller = BaseCommandsController(self.bot, UserService(UserDao(self.db)))

	def tearDown(self) -> None:
		self.db.close()
		os.remove("testing.sqlite")

	def test_start_handler(self):
		self.controller.start_handler(FakeMessage(self.test_chat_id, "/start", self.test_user))
		self.assertEqual("Привет! Я - бот для записи покупок.", self.bot.last_msg().text)
		self.assertEqual(self.test_chat_id, self.bot.last_msg().chat.id)
		row = self.db.fetchone("SELECT id, username FROM users WHERE id = ?", self.test_chat_id)
		self.assertTupleEqual((self.test_user["id"], self.test_user["username"]), row)

	def test_help_handler(self):
		self.controller.help_handler(FakeMessage(self.test_chat_id, "/help", self.test_user))
		self.assertEqual(help_message, self.bot.last_msg().text)
		self.assertEqual(self.test_chat_id, self.bot.last_msg().chat.id)
