import os
from datetime import datetime
from unittest import TestCase

from BotCore import DataBase
from parameterized import parameterized

import Config
from client.DataAnalysisController import DataAnalysisController
from services.income.IncomeDao import IncomeDao
from services.income.IncomeService import IncomeService
from services.purchase.PurchaseDao import PurchaseDao
from services.purchase.PurchaseInfo import PurchaseInfo
from services.purchase.PurchaseService import PurchaseService
from services.subscription.SubscriptionDao import SubscriptionDao
from services.subscription.SubscriptionService import SubscriptionService
from testing.TeleBotSpy import TeleBotSpy, FakeMessage
from testing.TestUtils import abs_path


class TestDataAnalysisController(TestCase):

	test_chat_id = 12345
	test_user = {"id": test_chat_id, "username": "test_username"}

	def setUp(self):
		self.bot = TeleBotSpy()
		self.db = DataBase("testing.sqlite", abs_path(Config.structure_path))
		self.purchase_service = PurchaseService(PurchaseDao(self.db))
		self.income_service = IncomeService(IncomeDao(self.db), self.purchase_service)
		subscription_service = SubscriptionService(SubscriptionDao(self.db), self.purchase_service)
		self.controller = DataAnalysisController(
			self.bot, self.purchase_service,
			self.income_service, subscription_service
		)

	def tearDown(self):
		self.db.close()
		os.remove("testing.sqlite")

	@parameterized.expand([
		(150, 4, None, -600.0),
		(150, 1, None, -150.0),
		(100, None, 1, -100.0),
		(100, None, 1.5, -150.0),
		(100, None, 0.5, -50.0)
	])
	def test_balance(self, price, count, weight, exp_balance):
		self.income_service.create(user_id=self.test_chat_id, title="test", income=100.0)
		for _ in range(count or 1):
			self.purchase_service.create(user_id=self.test_chat_id, title="test", price=price, weight=weight)
		self.controller.show_balance_handler(FakeMessage(self.test_chat_id, "/balance", self.test_user))
		self.assertEqual(self.bot.last_msg().text, f"Ваш баланс: {exp_balance + 100.0}{Config.currency_symbol}")

	@parameterized.expand([
		([
			 PurchaseInfo({"title": "test", "price": 150.3333333334})
		 ], [
			f"- test 150.33{Config.currency_symbol}"
		]),
		([
			 PurchaseInfo({"title": "test", "price": 150.0}),
			 PurchaseInfo({"title": "test", "price": 150.0})
		 ], [
			f"- test 150.0{Config.currency_symbol} x2 = 300.0{Config.currency_symbol}"
		]),
		([
			 PurchaseInfo({"title": "test", "price": 150.0}),
			 PurchaseInfo({"title": "test", "price": 155.5}),
			 PurchaseInfo({"title": "test2", "price": 155.5}),
		 ], [
			f"- test 150.0{Config.currency_symbol}",
			f"- test 155.5{Config.currency_symbol}",
			f"- test2 155.5{Config.currency_symbol}",
		]),
		([
			PurchaseInfo({"title": "test", "price": 150, "weight": 1.0})
		], [
			f"- test 150.0{Config.currency_symbol} 1.0кг"
		]),
		([
			PurchaseInfo({"title": "test", "price": 200, "weight": 1.5})
		], [
			f"- test 200.0{Config.currency_symbol} 1.5кг = 300.0{Config.currency_symbol}"
		]),
		([
			PurchaseInfo({"title": "test", "price": 200, "weight": 1.5}),
			 PurchaseInfo({"title": "test", "price": 200, "weight": 2.5})
		], [
			f"- test 200.0{Config.currency_symbol} 4.0кг = 800.0{Config.currency_symbol}"
		])
	])
	def test_purchases_view(self, purchases: list[PurchaseInfo], exp_strings: list[str]):
		for purchase in purchases:
			self.purchase_service.create(user_id=self.test_chat_id, title=purchase.title,
										 price=purchase.price, weight=purchase.weight)
		self.controller.show_purchases_handler(FakeMessage(self.test_chat_id, "/purchases", from_user=self.test_user))
		self.assertEqual("\n".join(exp_strings), self.bot.last_msg().text.split("\n\n")[1])

	@parameterized.expand([
		("/command 05.10.2022", datetime(2022, 10, 5), None),
		("/command 05.10", datetime(2022, 10, 5), None),
		("/command 05.10.2022 - 20.11.2023", datetime(2022, 10, 5), datetime(2023, 11, 20)),
		("/command 05.10 - 20.11", datetime(2022, 10, 5), datetime(2022, 11, 20)),
		("/command 05.10.2022 20:45", datetime(2022, 10, 5, 20, 45), None),
		("/command 20:45", datetime.now().replace(hour=20, minute=45, second=0, microsecond=0), None),
		("/command 12:00", datetime.now().replace(hour=12, minute=0, second=0, microsecond=0), None),
		("/command 05.10.2022 20:45 - 16:05", datetime(2022, 10, 5, 20, 45), datetime.now().replace(
			hour=16, minute=5, second=0, microsecond=0)),
		("/command 12:45 - 21:13", datetime.now().replace(hour=12, minute=45, second=0, microsecond=0),
		 datetime.now().replace(hour=21, minute=13, second=0, microsecond=0)),
	])
	def test_datetime_parsing(self, msg, exp_from_datetime: datetime, exp_to_datetime: datetime):
		from_ts, to_ts = DataAnalysisController.parse_datetime_range_from_message(msg)
		exp_from_timestamp = int(exp_from_datetime.timestamp()) if exp_from_datetime else None
		exp_to_timestamp = int(exp_to_datetime.timestamp()) if exp_to_datetime else None
		self.assertEqual(exp_from_timestamp, from_ts)
		self.assertEqual(exp_to_timestamp, to_ts)
