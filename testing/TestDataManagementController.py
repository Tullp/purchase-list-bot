import os
from unittest import TestCase

from BotCore import DataBase
from parameterized import parameterized

import Config
from client.DataManagementController import DataManagementController

from services.income.IncomeDao import IncomeDao
from services.income.IncomeService import IncomeService
from services.purchase.PurchaseDao import PurchaseDao
from services.purchase.PurchaseService import PurchaseService
from services.subscription.SubscriptionDao import SubscriptionDao
from services.subscription.SubscriptionService import SubscriptionService
from testing.TeleBotSpy import TeleBotSpy, FakeMessage, FakeCallbackQuery
from testing.TestUtils import abs_path


class TestDataManagementController(TestCase):

	test_chat_id = 12345
	test_user = {"id": test_chat_id, "username": "test_username"}

	def setUp(self):
		self.bot = TeleBotSpy()
		self.db = DataBase("testing.sqlite", abs_path(Config.structure_path))
		self.purchase_service = PurchaseService(PurchaseDao(self.db))
		income_service = IncomeService(IncomeDao(self.db), self.purchase_service)
		subscription_service = SubscriptionService(SubscriptionDao(self.db), self.purchase_service)
		self.controller = DataManagementController(
			self.bot, self.purchase_service,
			income_service, subscription_service
		)

	def tearDown(self) -> None:
		self.db.close()
		os.remove("testing.sqlite")

	@parameterized.expand([
		("/price test 10", "test", 10.0),
		("/price test1 test2 20.250", "test1 test2", 20.250),
		("/price test 10 250", "test 10", 250.0)
	])
	def test_price_recording(self, msg, exp_title, exp_price):
		self.controller.record_price_handler(FakeMessage(self.test_chat_id, msg, from_user=self.test_user))
		row = self.db.fetchone("SELECT title, price FROM prices WHERE title = ?", exp_title)
		self.assertTupleEqual((exp_title, exp_price), row)
		self.assertTrue(len(self.bot.get_data(self.test_chat_id, self.test_chat_id)) == 0)
		self.assertEqual(self.bot.last_msg().text, "Записано.")

	@parameterized.expand([
		("testing x5\n150", "testing", 5, None, 150.0),
		("testing х5\n150", "testing", 5, None, 150.0),
		("testing 5\n150", "testing 5", 1, None, 150.0),
		("testing\n150", "testing", 1, None, 150.0),
		("testing x1\n150", "testing", 1, None, 150.0),
		("testing testing x10\n20", "testing testing", 10, None, 20.0),
		("testing testing\n15", "testing testing", 1, None, 15.0),
		("testing testing x2 x5\n15", "testing testing x2", 5, None, 15.0),
		("testing x2\n150.524", "testing", 2, None, 150.524),
		("testing 5кг\n150", "testing", None, 5.0, 150.0),
		("testing 5kg\n150", "testing", None, 5.0, 150.0),
		("testing 5.5кг\n150", "testing", None, 5.5, 150.0)
	])
	def test_make_purchase_handler(self, msg, exp_title, exp_count, exp_weight, exp_price):
		self.controller.make_purchase_handler(FakeMessage(self.test_chat_id, msg, from_user=self.test_user))
		self.assertTrue(len(self.bot.get_data(self.test_chat_id, self.test_chat_id)) == 0)
		self.assertEqual(
			f"Записано {exp_count or (str(exp_weight) + 'кг')} {exp_title} по {exp_price}{Config.currency_symbol}.\n"
			f"Расходы за текущий месяц: {round(exp_price * (exp_count or exp_weight), 2)}{Config.currency_symbol}",
			self.bot.last_msg().text
		)
		row = self.db.fetchone("SELECT title, price, weight, COUNT(*) FROM purchases WHERE title = ?", exp_title)
		self.assertTupleEqual((exp_title, exp_price, exp_weight, exp_count or 1), row)

	@parameterized.expand([
		("testing x5", "150", "testing", 5, None, 150.0),
		("testing х5", "150", "testing", 5, None, 150.0),
		("testing", "150", "testing", 1, None, 150.0),
		("testing x1", "150", "testing", 1, None, 150.0),
		("testing testing x10", "20", "testing testing", 10, None, 20.0),
		("testing testing", "15", "testing testing", 1, None, 15.0),
		("testing testing x2 x5", "15", "testing testing x2", 5, None, 15.0),
		("testing x2", "150.524", "testing", 2, None, 150.524),
		("testing 5кг", "150", "testing", None, 5.0, 150.0),
		("testing 5kg", "150", "testing", None, 5.0, 150.0),
		("testing 5.5кг", "150", "testing", None, 5.5, 150.0)
	])
	def test_start_purchase_handler_and_custom_price_handler(
			self, msg1, msg2, exp_title, exp_count, exp_weight, exp_price):
		self.controller.start_purchase_handler(FakeMessage(self.test_chat_id, msg1, from_user=self.test_user))

		self.assertEqual("Напишите цену единицы товара. Отменить /cancel.", self.bot.last_msg().text)
		self.assertEqual(self.test_chat_id, self.bot.last_msg().chat.id)
		choose_message_id = self.bot.last_msg().message_id

		self.controller.custom_purchase_price_handler(FakeMessage(self.test_chat_id, msg2, from_user=self.test_user))

		row = self.db.fetchone("SELECT title, price, weight, COUNT(*) FROM purchases WHERE title = ?", exp_title)
		self.assertTupleEqual((exp_title, exp_price, exp_weight, exp_count or 1), row)
		self.assertTrue(len(self.bot.get_data(self.test_chat_id, self.test_chat_id)) == 0)
		self.assertTrue((self.test_chat_id, choose_message_id), self.bot.last_deleted_msg())
		total_amount = exp_count if exp_count else exp_weight
		self.assertEqual(
			f"Записано {exp_count or (str(exp_weight) + 'кг')} {exp_title} по {exp_price}{Config.currency_symbol}.\n"
			f"Расходы за текущий месяц: {round(exp_price * total_amount, 2)}{Config.currency_symbol}",
			self.bot.last_msg().text
		)

	@parameterized.expand([
		("testing x5", "testing", 5, None, 150.0),
		("testing х5", "testing", 5, None, 150.0),
		("testing", "testing", 1, None, 150.0),
		("testing x1", "testing", 1, None, 150.0),
		("testing testing x10", "testing testing", 10, None, 20.0),
		("testing testing", "testing testing", 1, None, 15.0),
		("testing testing x2 x5", "testing testing x2", 5, None, 15.0),
		("testing x2", "testing", 2, None, 150.524),
		("testing 5кг", "testing", None, 5.0, 150.0),
		("testing 5kg", "testing", None, 5.0, 150.0),
		("testing 5.5кг", "testing", None, 5.5, 150.0)
	])
	def test_start_purchase_handler_and_recommended_price_handler(
			self, msg, exp_title, exp_count, exp_weight, exp_price):
		self.purchase_service.create(user_id=self.test_user["id"], title=exp_title, price=exp_price, weight=exp_weight)
		self.controller.start_purchase_handler(FakeMessage(self.test_chat_id, msg, from_user=self.test_user))

		self.assertEqual(
			"Я нашел для тебя возможные цены на этот товар в базе. "
			"Ты можешь выбрать одну из них или написать свою. Отменить /cancel.",
			self.bot.last_msg().text
		)
		self.assertEqual(f"{float(exp_price)}{Config.currency_symbol}", self.bot.last_msg().reply_markup.keyboard[0][0].text)
		self.assertEqual(f"price {float(exp_price)}", self.bot.last_msg().reply_markup.keyboard[0][0].callback_data)
		self.assertEqual(DataManagementController.purchase_price_state, self.bot.get_state(self.test_chat_id))
		choose_message_id = self.bot.last_msg().message_id

		self.controller.recommended_price_handler(FakeCallbackQuery(
			self.test_chat_id, f"price {exp_price}", from_user=self.test_user, msg=self.bot.last_msg()))

		row = self.db.fetchone("SELECT title, price, weight, COUNT(*) FROM purchases WHERE title = ?", exp_title)
		self.assertTupleEqual((exp_title, exp_price, exp_weight, (exp_count or 1) + 1), row)
		self.assertTrue(len(self.bot.get_data(self.test_chat_id, self.test_chat_id)) == 0)
		self.assertTrue((self.test_chat_id, choose_message_id), self.bot.last_deleted_msg())
		self.assertEqual(None, self.bot.get_state(self.test_chat_id))
		total_amount = (exp_count + 1) if exp_count else exp_weight * 2
		self.assertEqual(
			f"Записано {exp_count or (str(exp_weight) + 'кг')} {exp_title} по {exp_price}{Config.currency_symbol}.\n"
			f"Расходы за текущий месяц: {round(exp_price * total_amount, 2)}{Config.currency_symbol}",
			self.bot.last_msg().text
		)

	def test_count_type_matching_checks_1(self):
		self.controller.make_purchase_handler(FakeMessage(self.test_chat_id, "test x5\n150", self.test_user))
		self.controller.make_purchase_handler(FakeMessage(self.test_chat_id, "test 5кг\n20", self.test_user))
		self.assertEqual(self.bot.last_msg().text, "По моим данным, этот товар не исчисляется в килограммах.")

	def test_count_type_matching_checks_2(self):
		self.controller.make_purchase_handler(FakeMessage(self.test_chat_id, "test 5кг\n150", self.test_user))
		self.controller.make_purchase_handler(FakeMessage(self.test_chat_id, "test x5\n20", self.test_user))
		self.assertEqual(self.bot.last_msg().text, "По моим данным, этот товар исчисляется в килограммах.")

	def test_subscription_creation(self):
		exp_title = "Test Subscription"
		exp_cost = 150.9
		exp_day = 20
		self.controller.create_subscription_handler(FakeMessage(self.test_chat_id, f"/subscription {exp_title}"))
		self.controller.subscription_cost_handler(FakeMessage(self.test_chat_id, str(exp_cost)))
		self.controller.subscription_day_handler(FakeMessage(self.test_chat_id, str(exp_day)))
		subscription = self.db.fetchone("SELECT user_id, title, cost, day FROM subscriptions")
		self.assertTupleEqual(subscription, (self.test_chat_id, exp_title, exp_cost, exp_day))
