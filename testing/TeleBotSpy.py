import random
from typing import Union, Optional, List

from BotCore import Bot
from telebot import types, REPLY_MARKUP_TYPES
from telebot.storage import StateContext
from telebot.types import Message, Chat, User, CallbackQuery, InlineKeyboardMarkup


class FakeMessage(Message):

	def __init__(self, chat_id, text, from_user=None, reply_markup=None):
		super().__init__(random.randint(10000, 999999), None, None, Chat(chat_id, None), None, [], None)
		self.text: str = text
		self.reply_markup: InlineKeyboardMarkup = reply_markup
		if from_user is not None:
			self.from_user: User = User(from_user["id"], False, None, username=from_user["username"])
		else: self.from_user: User = User(chat_id, False, None)


class FakeCallbackQuery(CallbackQuery):

	def __init__(self, chat_id, data, from_user=None, msg=None):
		super().__init__(None, None, data, Chat(chat_id, None), message=msg)
		if from_user is not None:
			self.from_user = User(from_user["id"], False, None, username=from_user["username"])


class TeleBotSpy(Bot):

	def __init__(self):
		super().__init__("fake token")
		self.messages: list[FakeMessage] = []
		self.deleted_messages: list[tuple[int, int]] = []
		self.data = {}
		self._reset_data = {}

	def last_msg(self):
		return self.messages[-1]

	def last_deleted_msg(self):
		return self.deleted_messages[-1]

	def send_message(self, chat_id: Union[int, str], text: str, parse_mode: Optional[str] = None,
					 entities: Optional[List[types.MessageEntity]] = None,
					 disable_web_page_preview: Optional[bool] = None, disable_notification: Optional[bool] = None,
					 protect_content: Optional[bool] = None, reply_to_message_id: Optional[int] = None,
					 allow_sending_without_reply: Optional[bool] = None,
					 reply_markup: Optional[REPLY_MARKUP_TYPES] = None, timeout: Optional[int] = None) -> types.Message:
		self.messages.append(FakeMessage(chat_id, text, reply_markup=reply_markup))
		return self.last_msg()

	def delete_message(self, chat_id: Union[int, str], message_id: int, timeout: Optional[int] = None) -> bool:
		self.deleted_messages.append((chat_id, message_id))
		return True

	def set_state(self, user_id: int, state: Union[int, str], chat_id: int = None) -> None:
		self.data.setdefault(user_id, {}).setdefault(user_id, {}).update({"state": state})

	def get_state(self, user_id: int, chat_id: int = None):
		return self.data.get(user_id, {}).get(user_id, {}).get("state")

	def delete_state(self, user_id: int, chat_id: int = None):
		self.data.get(user_id, {}).get(user_id, {}).update({"state": None})

	def get_data(self, chat_id, user_id):
		return self.data.get(user_id, {}).get(chat_id, {}).get("data", {})

	def save(self, chat_id, user_id, data):
		self.data.setdefault(user_id, {}).setdefault(chat_id, {}).update({"data": data})

	def add_data(self, user_id: int, chat_id: int = None, **kwargs):
		self.data.setdefault(user_id, {}).setdefault(chat_id, {}).setdefault("data", {}).update(kwargs)

	def retrieve_data(self, user_id: int, chat_id: int = None):
		return StateContext(self, chat_id, user_id)

	def reset_data(self, user_id: int, chat_id: int = None):
		if self.data.get(user_id):
			if self.data[user_id].get(chat_id):
				self.data[user_id][chat_id]['data'] = {}
