import sqlite3

from BotCore import Controller, Bot, message_handler
from telebot.types import Message

from Config import help_message, tullp
from services.user.UserService import UserService


class BaseCommandsController(Controller):

	def __init__(self, bot: Bot, user_service: UserService):
		super().__init__(bot)
		self.user_service = user_service

	@message_handler(commands=["start"], chat_types=["private"])
	def start_handler(self, message: Message):
		try:
			self.user_service.create(id=message.from_user.id, username=message.from_user.username)
		except sqlite3.IntegrityError:
			pass
		self.bot.send_message(message.chat.id, "Привет! Я - бот для записи покупок.")

	@message_handler(commands=["help"], chat_types=["private"])
	def help_handler(self, message: Message):
		self.bot.send_message(message.chat.id, help_message)

	@message_handler(commands=["sqlite"], func=lambda msg: msg.chat.id == tullp)
	def sqlite_handler(self, message: Message):
		query = message.text.split(" ", 1)[1]
		if query.lower().startswith("select"):
			rows = self.user_service.dao.database.fetchall(query)
			self.bot.send_message(tullp, "Result:\n\n" + "\n".join(" ".join(map(str, row)) for row in rows))
		else:
			rowid = self.user_service.dao.database.update(query)
			self.bot.send_message(tullp, f"Done. Rowid: {rowid}")
