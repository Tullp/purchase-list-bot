
import traceback

from BotCore import Bot, DataBase
from telebot.custom_filters import StateFilter

from Config import tullp, dbpath, structure_path
from client.BaseCommandsController import BaseCommandsController
from client.DataAnalysisController import DataAnalysisController
from client.DataManagementController import DataManagementController
from server.server import Server
from services.income.IncomeDao import IncomeDao
from services.income.IncomeService import IncomeService
from services.purchase.PurchaseDao import PurchaseDao
from services.purchase.PurchaseService import PurchaseService
from services.subscription.SubscriptionDao import SubscriptionDao
from services.subscription.SubscriptionService import SubscriptionService
from services.user.UserDao import UserDao
from services.user.UserService import UserService


class PurchaseBot(Bot):

	def __init__(self, token: str):
		super().__init__(token)
		self.enable_saving_states()
		self.add_custom_filter(StateFilter(self))

	def init_controllers(self):
		db = DataBase(dbpath, structure_path)
		user_service = UserService(UserDao(db))
		purchase_service = PurchaseService(PurchaseDao(db))
		income_service = IncomeService(IncomeDao(db), purchase_service)
		subscription_service = SubscriptionService(SubscriptionDao(db), purchase_service)

		BaseCommandsController(self, user_service)
		DataAnalysisController(self, purchase_service, income_service, subscription_service)
		DataManagementController(self, purchase_service, income_service, subscription_service)

		subscription_service.start(self)
		Server(db).start()

	def handle(self, e):
		print(traceback.format_exc())
		self.send_message(tullp, traceback.format_exc())
		return True
