
import re
from typing import Any

from BotCore import Controller, Bot, message_handler, callback_query_handler
from telebot.types import InlineKeyboardMarkup, InlineKeyboardButton, Message, CallbackQuery

from Config import currency_symbol, tullp
from services.income.IncomeService import IncomeService
from services.purchase.PurchaseInfo import PurchaseInfo
from services.purchase.PurchaseService import PurchaseService
from services.subscription.SubscriptionService import SubscriptionService


class DataManagementController(Controller):

	purchase_price_state = "purchase_price_state"
	subscription_cost_state = "subscription_cost_state"
	subscription_day_state = "subscription_day_state"

	@staticmethod
	def parse_purchase_title(title) -> dict:
		if match := re.match("^(.+?) [xх](\\d+)$", title):
			title, count = match.groups()
			return {"title": title.strip(), "count": int(count), "weight": None}
		elif match := re.match("^(.+?) (\\d+(?:\\.\\d+)?)(?:кг|kg)$", title):
			title, weight = match.groups()
			return {"title": title.strip(), "count": None, "weight": float(weight)}
		else:
			return {"title": title.strip(), "count": None, "weight": None}

	def __init__(
			self, bot: Bot, purchase_service: PurchaseService,
			income_service: IncomeService,
			subscription_service: SubscriptionService):
		super().__init__(bot)
		self.purchase_service = purchase_service
		self.income_service = income_service
		self.subscription_service = subscription_service

	@message_handler(commands=["delete_purchase"], chat_types=["private"])
	def delete_purchase_handler(self, message: Message):
		purchase_id = int(message.text.split(" ", 1)[1])
		self.purchase_service.delete(purchase_id)
		self.bot.send_message(message.chat.id, "Удалено.")

	@message_handler(regexp="^\\+(\\d+(?:\\.\\d+)?)(?:\\s+(.+)\\s*)?$", chat_types=["private"])
	def income_recording_handler(self, message: Message):
		income, title = re.match("^\\+(\\d+(?:\\.\\d+)?)(?:\\s+(.+))?$", message.text).groups()
		self.income_service.create(user_id=message.from_user.id, title=title, income=float(income))
		if title is not None:
			self.bot.send_message(message.from_user.id, f"Записан доход {title} {income}{currency_symbol}")
		else:
			self.bot.send_message(message.from_user.id, f"Записан доход в {income}{currency_symbol}")

	@message_handler(commands=["del_subscription"], chat_types=["private"])
	def delete_subscription_handler(self, message: Message):
		title = message.text.split(" ", 1)[1]
		self.subscription_service.remove_by_title(message.from_user.id, title)
		self.bot.send_message(message.from_user.id, "Готово")

	@message_handler(regexp="^/subscription (.+)$", chat_types=["private"])
	def create_subscription_handler(self, message: Message):
		title = message.text.split(" ", 1)[1]
		self.bot.set_state(message.from_user.id, self.subscription_cost_state)
		self.bot.add_data(message.from_user.id, message.chat.id, title=title)
		self.bot.send_message(message.from_user.id, "Введите цену регулярного платежа")

	@message_handler(regexp="^\\d+(?:\\.\\d+)?$", state=subscription_cost_state)
	def subscription_cost_handler(self, message: Message):
		self.bot.add_data(message.from_user.id, message.chat.id, cost=float(message.text))
		self.bot.set_state(message.from_user.id, self.subscription_day_state)
		self.bot.send_message(message.from_user.id, "Введите день осуществления регулярного платежа")

	@message_handler(regexp="^\\d+$", state=subscription_day_state)
	def subscription_day_handler(self, message: Message):
		day = int(message.text)
		if day > 28:
			self.bot.send_message(message.from_user.id, "День месяца должен быть 28 или ниже.")
		else:
			with self.bot.retrieve_data(message.from_user.id, message.chat.id) as d:
				self.subscription_service.create(
					user_id=message.from_user.id, title=d["title"], cost=d["cost"], day=day)
			self.bot.delete_state(message.from_user.id, message.chat.id)
			self.bot.send_message(message.from_user.id, "Готово!")

	@message_handler(commands=["cancel"], state=[subscription_cost_state, subscription_day_state])
	def cancel_subscription_input_handler(self, message: Message):
		self.bot.delete_state(message.from_user.id)
		self.bot.send_message(message.chat.id, "Отменено.")

	@message_handler(content_types=["text"], state=subscription_cost_state)
	def invalid_cost_handler(self, message: Message):
		self.bot.send_message(
			message.chat.id,
			"Введите число без посторонних символов. "
			"Дробная часть отделяется точкой. Отменить /cancel."
		)

	@message_handler(content_types=["text"], state=subscription_day_state)
	def invalid_day_handler(self, message: Message):
		self.bot.send_message(message.chat.id, "Введите число без посторонних символов. Отменить /cancel.")

	@message_handler(regexp="^\\d+(?:\\.\\d+)?$", state=purchase_price_state)
	def custom_purchase_price_handler(self, message: Message):
		price: float = float(message.text)
		self.bot.add_data(message.from_user.id, message.chat.id, price=price)
		with self.bot.retrieve_data(message.from_user.id, message.chat.id) as d:
			data = d
		self.make_purchase(message.from_user.id, data)

	@message_handler(commands=["cancel"], state=purchase_price_state)
	def cancel_purchase_input_handler(self, message: Message):
		with self.bot.retrieve_data(message.from_user.id, message.chat.id) as data:
			try:
				self.bot.delete_message(message.from_user.id, data["message_id"])
			except Exception:
				pass
		self.bot.delete_state(message.from_user.id)
		self.bot.send_message(message.chat.id, "Отменено.")

	@message_handler(content_types=["text"], state=purchase_price_state)
	def invalid_price_handler(self, message: Message):
		self.bot.send_message(
			message.chat.id,
			"Введите число без посторонних символов. "
			"Дробная часть отделяется точкой. Отменить /cancel."
		)

	@message_handler(regexp="^.+\\n\\d+(\\.\\d+)?$", chat_types=["private"])
	def make_purchase_handler(self, message: Message):
		purchase_params = self.parse_purchase_title(message.text.split("\n")[0])
		price = float(message.text.split("\n")[1])
		self.make_purchase(message.from_user.id, {**purchase_params, "price": price})

	@message_handler(regexp="^(.+?)(?: [xх](\\d+))?$", chat_types=["private"])
	def start_purchase_handler(self, message: Message):

		purchase_params = self.parse_purchase_title(message.text)
		self.bot.set_state(message.from_user.id, self.purchase_price_state)
		self.bot.add_data(message.from_user.id, message.chat.id, **purchase_params)

		recommendations = self.purchase_service.get_price_recommendations(
			message.from_user.id, purchase_params["title"])
		if len(recommendations) > 0:
			markup = InlineKeyboardMarkup()
			markup.add(*[InlineKeyboardButton(
				f"{recommendation}{currency_symbol}",
				callback_data=f"price {recommendation}"
			) for recommendation in recommendations], row_width=3)
			msg = self.bot.send_message(
				message.chat.id,
				"Я нашел для тебя возможные цены на этот товар в базе. "
				"Ты можешь выбрать одну из них или написать свою. Отменить /cancel.",
				reply_markup=markup
			)
		else:
			msg = self.bot.send_message(message.chat.id, "Напишите цену единицы товара. Отменить /cancel.")
		self.bot.add_data(message.from_user.id, message.chat.id, message_id=msg.message_id)

	@callback_query_handler(func=lambda call: call.data.startswith("price"))
	def recommended_price_handler(self, call: CallbackQuery):
		with self.bot.retrieve_data(call.from_user.id, call.message.chat.id) as d:
			data = d
		if data["message_id"] != call.message.message_id:
			self.bot.edit_message_text("Устаревшая покупка.", call.message.chat.id, call.message.message_id)
			return

		data["price"] = float(call.data.split(" ")[1])
		self.make_purchase(call.from_user.id, data)

	@callback_query_handler(func=lambda call: call.data.startswith("delete_purchase"))
	def delete_purchases_handler(self, call: CallbackQuery):
		purchases: list[str] = call.data.split()[1:]
		for purchase_id in purchases:
			self.purchase_service.delete(int(purchase_id))
		self.bot.edit_message_text(f"{call.message.text}\n\nУдалено.", call.from_user.id, call.message.message_id)

	def make_purchase(self, user_id: int, data: dict[str, Any]):
		is_item_countable = self.purchase_service.is_item_countable(user_id, data["title"])
		if data["count"] is None and data["weight"] is None:
			data["count" if is_item_countable in [True, None] else "weight"] = 1
		if not self.purchase_service.check_count_type_matching(user_id, PurchaseInfo(data)):
			if is_item_countable:
				self.bot.send_message(user_id, "По моим данным, этот товар не исчисляется в килограммах.")
			else:
				self.bot.send_message(user_id, "По моим данным, этот товар исчисляется в килограммах.")
		else:
			id_list = [
				self.purchase_service.create(
					user_id=user_id,
					title=data["title"],
					price=data["price"],
					weight=data["weight"]
				)
				for _ in range(data["count"] or 1)
			]
			markup = InlineKeyboardMarkup()
			markup.add(InlineKeyboardButton("Удалить", callback_data="delete_purchase " + " ".join(map(str, id_list))))
			monthly_expenses: float = self.purchase_service.get_monthly_expenses(user_id)
			amount_string = data['count'] or (str(data['weight']) + 'кг')
			self.bot.send_message(
				user_id,
				f"Записано {amount_string} {data['title']} по {data['price']}{currency_symbol}.\n"
				f"Расходы за текущий месяц: {monthly_expenses}{currency_symbol}",
				reply_markup=markup
			)
		self.bot.reset_data(user_id, user_id)
		self.bot.delete_state(user_id)
		if data.get("message_id"):
			try:
				self.bot.delete_message(user_id, data["message_id"])
			except Exception:
				pass
