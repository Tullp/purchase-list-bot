import datetime
import re
from typing import Optional

from BotCore import Controller, Bot, message_handler
from telebot.types import Message

from Config import currency_symbol
from services.income.IncomeService import IncomeService
from services.purchase.PurchaseGroupInfo import PurchaseGroupInfo
from services.purchase.PurchaseInfo import PurchaseInfo
from services.purchase.PurchaseService import PurchaseService
from services.subscription.SubscriptionService import SubscriptionService


class DataAnalysisController(Controller):

	@classmethod
	def convert_purchases_to_view(cls, purchases: list[PurchaseInfo]) -> str:
		return cls.convert_grouped_purchases_to_view(
			PurchaseService.group_purchases_by_title_and_price(purchases))

	@classmethod
	def convert_grouped_purchases_to_view(cls, grouped_purchases: list[PurchaseGroupInfo]) -> str:
		def group_to_view(group: PurchaseGroupInfo) -> str:
			view = f"- {group.title} {round(group.price, 2)}{currency_symbol}"
			if group.weight is not None:
				amount = group.weight
				view += f" {group.weight}кг"
			else:
				amount = group.count
				if group.count != 1:
					view += f" x{group.count}"
			if amount != 1:
				view += f" = {round(group.price * amount, 2)}{currency_symbol}"
			return view
		return "\n".join(group_to_view(group) for group in grouped_purchases)

	@classmethod
	def parse_datetime_range_from_message(cls, text: str) -> tuple[int, int]:
		parts = text.split(" - ")
		from_timestamp = cls.parse_datetime(parts[0])
		to_timestamp = cls.parse_datetime(parts[1]) if len(parts) > 1 else None
		return from_timestamp, to_timestamp

	@classmethod
	def parse_datetime(cls, text: str) -> Optional[int]:
		datetime_params = {}

		if match := re.search("(\\d{1,2})\\.(\\d{1,2})(?:\\.(\\d{4}))?", text):
			datetime_params.update({
				"year": int(match.group(3)) if match.group(3) else None,
				"month": int(match.group(2)),
				"day": int(match.group(1))
			})
		if match := re.search("(\\d{1,2}):(\\d{1,2})", text):
			hour, minute = map(int, match.groups())
			datetime_params.update({"hour": hour, "minute": minute})

		if len(datetime_params) > 0:

			now = datetime.datetime.now()
			if not datetime_params.get("year"):
				datetime_params.update({"year": now.year})
			if "day" not in datetime_params:
				datetime_params.update({"month": now.month, "day": now.day})

			return int(datetime.datetime(**datetime_params).timestamp())

	def send_purchases_list(self, user_id: int, from_timestamp, to_timestamp):
		purchases = self.purchase_service.get_purchases(user_id, from_timestamp, to_timestamp)
		total_sum = sum(p.price * (p.weight or 1) for p in purchases)
		self.bot.send_message(
			user_id,
			f"Покупки с {datetime.datetime.fromtimestamp(from_timestamp).strftime('%d %h %Y %H:%M')} "
			f"({round(total_sum, 2)}{currency_symbol}):\n\n"
			+ self.convert_purchases_to_view(purchases)
		)

	def __init__(
			self, bot: Bot, purchase_service: PurchaseService,
			income_service: IncomeService, subscription_service: SubscriptionService):
		super().__init__(bot)
		self.purchase_service = purchase_service
		self.income_service = income_service
		self.subscription_service = subscription_service

	@message_handler(commands=["find"], chat_types=["private"])
	def find_purchases_handler(self, message: Message):
		title_part = message.text.split(" ", 1)[1]
		purchases = self.purchase_service.find_by_title_part(message.from_user.id, title_part)
		self.bot.send_message(message.chat.id, "Я нашел следующие покупки:\n\n" + "\n".join(
			f"{info.id}. {info.title} {info.price}{currency_symbol}" for info in purchases
		))

	@message_handler(commands=["last"], chat_types=["private"])
	def show_last_purchases_handler(self, message: Message):
		last_purchase = self.purchase_service.get_last_purchases(message.from_user.id)
		total_sum = sum(p.price * (p.weight or 1) for p in last_purchase)
		self.bot.send_message(
			message.from_user.id,
			f"Последние покупки ({round(total_sum, 2)}{currency_symbol}):\n\n"
			+ self.convert_purchases_to_view(last_purchase)
		)

	@message_handler(commands=["purchases", "show"], chat_types=["private"])
	def show_purchases_handler(self, message: Message):

		try:
			from_timestamp, to_timestamp = self.parse_datetime_range_from_message(message.text)
		except Exception as error:
			self.bot.send_message(message.chat.id, str(error))
			return
		if from_timestamp is None:
			from_timestamp = int(datetime.datetime.now().replace(day=1, hour=0, minute=0).timestamp())
		if to_timestamp is None:
			to_timestamp = int(datetime.datetime.now().timestamp())
		self.send_purchases_list(message.from_user.id, from_timestamp, to_timestamp)

	@message_handler(commands=["history"], chat_types=["private"])
	def show_history_handler(self, message: Message):
		purchases = self.purchase_service.get_history(message.from_user.id)

		def convert_to_history_row(p: PurchaseInfo):
			if p.weight is None:
				return f"{p.title} - {p.price}{currency_symbol}"
			else:
				return f"{p.title} {p.weight}кг - {p.price * p.weight}{currency_symbol}"

		self.bot.send_message(message.chat.id, "History:\n\n" + "\n".join(
			convert_to_history_row(p) for p in purchases
		))

	@message_handler(commands=["incomes"], chat_types=["private"])
	def show_incomes_handler(self, message: Message):
		incomes = self.income_service.get_monthly_incomes(message.from_user.id)
		total_sum = sum(info.income for info in incomes)
		self.bot.send_message(
			message.from_user.id,
			f"Месячные доходы ({round(total_sum, 2)}{currency_symbol}):\n\n"
			+ IncomeService.convert_incomes_to_view(incomes)
		)

	@message_handler(commands=["subscriptions"], chat_types=["private"])
	def show_subscriptions_handler(self, message: Message):
		now = datetime.datetime.now()
		cur_month = now.strftime("%B")
		next_month = (now + datetime.timedelta(days=30)).strftime("%B")

		subscriptions = self.subscription_service.user_subscriptions(message.from_user.id)
		subscriptions.sort(key=lambda sub: (sub.day - 32) if now.day <= sub.day else sub.day)

		self.bot.send_message(message.from_user.id, "Subscriptions:\n\n" + "\n".join(
			f"{sub.day} {cur_month if now.day <= sub.day else next_month}: "
			f"{sub.title} {sub.cost}{currency_symbol}"
			for sub in subscriptions
		))

	@message_handler(commands=["balance"], chat_types=["private"])
	def show_balance_handler(self, message: Message):
		balance = self.income_service.get_balance(message.from_user.id)
		self.bot.send_message(message.from_user.id, f"Ваш баланс: {balance}{currency_symbol}")
