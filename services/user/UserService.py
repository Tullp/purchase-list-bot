
from BotCore import Service

from services.user.UserDao import UserDao
from services.user.UserInfo import UserInfo


class UserService(Service[UserInfo, UserDao]):

	def __init__(self, dao: UserDao):
		super().__init__(UserInfo, dao)
