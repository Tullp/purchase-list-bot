from BotCore import ModelInfo


class UserInfo(ModelInfo):

	id: int
	username: str

	@classmethod
	def table(cls):
		return "users"

	@classmethod
	def fields(cls):
		return "id", "username"
