from BotCore import DAO, DataBase

from services.user.UserInfo import UserInfo


class UserDao(DAO):

	def __init__(self, database: DataBase):
		super().__init__(database, UserInfo)
