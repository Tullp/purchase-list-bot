from BotCore import DAO, DataBase

from services.purchase.PurchaseInfo import PurchaseInfo


class PurchaseDao(DAO):

	def __init__(self, database: DataBase):
		super().__init__(database, PurchaseInfo)

	def find_by_title_part(self, user_id: int, title_part: str) -> list[tuple]:
		return self.database.fetchall(
			"SELECT * FROM purchases "
			"WHERE user_id = ? and lower(title) LIKE ?"
			"ORDER BY [timestamp] DESC",
			user_id, f"%{title_part.lower()}%"
		)

	def find_by_title(self, user_id: int, title: str) -> list[tuple]:
		return self.database.fetchall(
			"SELECT * FROM purchases "
			"WHERE lower(title) = ? and user_id = ? "
			"ORDER BY [timestamp] DESC",
			title.lower(), user_id
		)

	def filter(self, user_id: int, from_timestamp: int, to_timestamp: int) -> list[tuple]:
		return self.database.fetchall(
			"SELECT * FROM purchases "
			"WHERE user_id = ? and [timestamp] >= ? and [timestamp] <= ?"
			"ORDER BY [timestamp] DESC",
			user_id, from_timestamp, to_timestamp
		)

	def get_total_purchases_cost(self, user_id: int) -> float:
		return self.database.fetchone(
			"SELECT SUM(price * IIF(weight is not null, weight, 1)) "
			"FROM purchases WHERE user_id = ?", user_id
		)[0] or 0

	def get_last(self, user_id: int) -> tuple:
		return self.database.fetchone(
			"SELECT * FROM purchases WHERE user_id = ? "
			"ORDER BY [timestamp] DESC LIMIT 1", user_id
		)

	def get_titles(self, user_id: int) -> list[str]:
		rows = self.database.fetchall("SELECT DISTINCT title FROM purchases WHERE user_id = ?", user_id)
		return [row[0] for row in rows]
