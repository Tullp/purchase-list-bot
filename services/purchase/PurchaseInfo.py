
from BotCore import ModelInfo


class PurchaseInfo(ModelInfo):

	id: int
	user_id: int
	title: str
	price: float
	weight: float
	timestamp: int

	@classmethod
	def table(cls):
		return "purchases"

	@classmethod
	def fields(cls):
		return "id", "user_id", "title", "price", "weight", "timestamp"
