
class PurchaseGroupInfo:

	title: str
	price: float
	weight: float
	count: int

	def __init__(self, data):
		self.title = data["title"]
		self.price = data["price"]
		self.weight = data["weight"]
		self.count = data["count"]
