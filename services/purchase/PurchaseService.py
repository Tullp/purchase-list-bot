
from datetime import datetime, timedelta
from typing import Optional

from BotCore import Service

from services.purchase.PurchaseDao import PurchaseDao
from services.purchase.PurchaseGroupInfo import PurchaseGroupInfo
from services.purchase.PurchaseInfo import PurchaseInfo


class PurchaseService(Service[PurchaseInfo, PurchaseDao]):

	@staticmethod
	def group_purchases_by_title_and_price(purchases: list[PurchaseInfo]) -> list[PurchaseGroupInfo]:
		identifiers = list(dict.fromkeys(f"{info.title.lower()}|{info.price}" for info in purchases))
		groups = [
			[info for info in purchases if f"{info.title.lower()}|{info.price}" == identifier]
			for identifier in identifiers
		]
		return [
			PurchaseGroupInfo({
				"title": group[0].title,
				"price": group[0].price,
				"weight": sum(g.weight for g in group) if group[0].weight is not None else None,
				"count": len(group)
			}) for group in groups
		]

	def __init__(self, dao: PurchaseDao):
		super().__init__(PurchaseInfo, dao)

	def create(self, **kwargs):
		kwargs.update({"timestamp": int(datetime.now().timestamp())})
		if not self.check_count_type_matching(kwargs.get("user_id"), PurchaseInfo(kwargs)):
			raise Exception("Invalid count type.")
		return super().create(**kwargs)

	def check_count_type_matching(self, user_id: int, purchase: PurchaseInfo):
		is_item_countable = self.is_item_countable(user_id, purchase.title)
		return is_item_countable is None or is_item_countable == (purchase.weight is None)

	def is_item_countable(self, user_id: int, title: str) -> Optional[bool]:
		purchases = self.find_by_title(user_id, title)
		if len(purchases) == 0:
			return None
		return purchases[0].weight is None

	def find_by_title_part(self, user_id: int, title_part: str) -> list[PurchaseInfo]:
		return self.to_objects(self.dao.find_by_title_part(user_id, title_part))

	def find_by_title(self, user_id: int, title_part: str) -> list[PurchaseInfo]:
		return self.to_objects(self.dao.find_by_title(user_id, title_part))

	def get_purchases(self, user_id: int, from_timestamp: int, to_timestamp: int):
		return self.to_objects(self.dao.filter(user_id, from_timestamp, to_timestamp))

	def get_monthly_purchases(self, user_id: int):
		month_start = int(datetime.now().replace(day=1, hour=0, minute=0).timestamp())
		now = int(datetime.now().timestamp())
		return self.to_objects(self.dao.filter(user_id, month_start, now))

	def get_monthly_expenses(self, user_id: int):
		return round(sum(p.price * (p.weight or 1) for p in self.get_monthly_purchases(user_id)), 2)

	def get_total_purchases_cost(self, user_id: int):
		return self.dao.get_total_purchases_cost(user_id)

	def get_last_purchase(self, user_id: int):
		return self.to_object(self.dao.get_last(user_id))

	def get_last_purchases(self, user_id: int, grouping: bool = False):
		last_purchase = self.get_last_purchase(user_id)
		from_timestamp = last_purchase.timestamp - 3600
		to_timestamp = int(datetime.now().timestamp())
		purchases = self.get_purchases(user_id, from_timestamp, to_timestamp)
		if grouping:
			purchases = self.group_purchases_by_title_and_price(purchases)
		return purchases

	def get_history(self, user_id):
		from_timestamp = int((datetime.now() - timedelta(days=30)).timestamp())
		to_timestamp = int(datetime.now().timestamp())
		return self.get_purchases(user_id, from_timestamp, to_timestamp)

	def get_price_recommendations(self, user_id: int, title: str) -> list[float]:
		all_prices = self.to_objects(self.dao.find_by_title(user_id, title))
		groups = self.group_purchases_by_title_and_price(all_prices)
		prices_info = [{"price": g.price, "frequency": g.count} for g in groups]
		group_by_frequency = [{
			"frequency": frequency,
			"prices": [info["price"] for info in prices_info if info["frequency"] == frequency]
		} for frequency in list(dict.fromkeys(info["frequency"] for info in prices_info))]
		# Sorts prices in order of their frequencies
		group_by_frequency.sort(key=lambda group: group["frequency"], reverse=True)
		for frequency_group in group_by_frequency:
			average = sum(frequency_group["prices"]) / len(frequency_group["prices"])
			# Sorts prices within a group with the same frequency
			# in ascending order of the difference with its arithmetic mean
			frequency_group["prices"].sort(key=lambda price: abs(price - average))
		prices = sum([frequency_group["prices"] for frequency_group in group_by_frequency], [])
		return prices[:6]

	def get_titles(self, user_id):
		return self.dao.get_titles(user_id)
