from datetime import datetime

from BotCore import Service

from Config import currency_symbol

from services.income.IncomeDao import IncomeDao
from services.income.IncomeInfo import IncomeInfo
from services.purchase.PurchaseService import PurchaseService


class IncomeService(Service[IncomeInfo, IncomeDao]):

	@staticmethod
	def convert_incomes_to_view(incomes: list[IncomeInfo]) -> str:
		income_without_title = sum(info.income for info in incomes if info.title is None)
		return f"- Other: {income_without_title}{currency_symbol}\n" + "\n".join(
			f"- {info.title}: {info.income}{currency_symbol}"
			for info in incomes if info.title is not None
		)

	def __init__(self, dao: IncomeDao, purchase_service: PurchaseService):
		super().__init__(IncomeInfo, dao)
		self.purchase_service = purchase_service

	def create(self, **kwargs):
		kwargs.update({"timestamp": int(datetime.now().timestamp())})
		return super().create(**kwargs)

	def get_monthly_incomes(self, user_id: int) -> list[IncomeInfo]:
		return self.to_objects(self.dao.get_monthly_incomes(user_id))

	def get_user_incomes(self, user_id: int) -> list[IncomeInfo]:
		return self.to_objects(self.dao.get_incomes(user_id))

	def get_balance(self, user_id: int) -> float:
		return round(self.dao.get_total_income(user_id) - self.purchase_service.get_total_purchases_cost(user_id), 2)
