
from BotCore import ModelInfo


class IncomeInfo(ModelInfo):

	id: int
	user_id: int
	title: str
	income: float
	timestamp: int

	@classmethod
	def table(cls):
		return "incomes"

	@classmethod
	def fields(cls):
		return "id", "user_id", "title", "income", "timestamp"
