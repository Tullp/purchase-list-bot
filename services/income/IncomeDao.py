from datetime import datetime

from BotCore import DAO, DataBase

from services.income.IncomeInfo import IncomeInfo


class IncomeDao(DAO):

	def __init__(self, database: DataBase):
		super().__init__(database, IncomeInfo)

	def get_incomes(self, user_id: int):
		return self.database.fetchall("SELECT * FROM incomes WHERE user_id = ? ORDER BY [timestamp] DESC", user_id)

	def get_monthly_incomes(self, user_id: int):
		return self.database.fetchall(
			"SELECT * FROM incomes "
			"WHERE user_id = ? and [timestamp] >= ? "
			"ORDER BY [timestamp] DESC",
			user_id, datetime.now().replace(day=1, hour=0, minute=0, second=0).timestamp()
		)

	def get_total_income(self, user_id: int) -> float:
		return self.database.fetchone("SELECT SUM(income) FROM incomes WHERE user_id = ?", user_id)[0] or 0
