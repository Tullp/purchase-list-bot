import datetime
import threading
import time

import schedule
from BotCore import Service, Bot

from Config import currency_symbol
from services.purchase.PurchaseService import PurchaseService
from services.subscription.SubscriptionDao import SubscriptionDao
from services.subscription.SubscriptionInfo import SubscriptionInfo


class SubscriptionService(Service[SubscriptionInfo, SubscriptionDao]):

	def __init__(self, dao, purchase_service: PurchaseService):
		super().__init__(SubscriptionInfo, dao)
		self.purchase_service = purchase_service

	def remove_by_title(self, user_id: int, title: str):
		self.dao.remove_by_title(user_id, title)

	def user_subscriptions(self, user_id: int):
		return self.to_objects(self.dao.user_subscriptions(user_id))

	def start(self, bot: Bot):
		schedule.every().day.at("10:00").do(self.subscription_processing, bot)
		threading.Thread(target=self.run_pending, name="pending").start()

	def subscription_processing(self, bot: Bot):
		today = datetime.date.today()
		for sub in self.getall():
			if sub.day == today.day:
				self.purchase_service.create(
					user_id=sub.user_id,
					title=sub.title,
					price=sub.cost
				)
				bot.send_message(sub.user_id, f"Регулярный платеж: {sub.title} {sub.cost}{currency_symbol}")

	@staticmethod
	def run_pending():
		while True:
			schedule.run_pending()
			time.sleep(1)
