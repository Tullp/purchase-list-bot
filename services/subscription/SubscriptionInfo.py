from BotCore import ModelInfo


class SubscriptionInfo(ModelInfo):

	id: int
	user_id: int
	title: str
	cost: float
	day: int

	@classmethod
	def table(cls):
		return "subscriptions"

	@classmethod
	def fields(cls):
		return "id", "user_id", "title", "cost", "day"
