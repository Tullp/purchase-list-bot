from BotCore import DAO, DataBase

from services.subscription.SubscriptionInfo import SubscriptionInfo


class SubscriptionDao(DAO):

	def __init__(self, database: DataBase):
		super().__init__(database, SubscriptionInfo)

	def remove_by_title(self, user_id: int, title: str):
		self.database.update("DELETE FROM subscriptions WHERE user_id = ? and title = ?", user_id, title)

	def user_subscriptions(self, user_id: int):
		return self.database.fetchall("SELECT * FROM subscriptions WHERE user_id = ? ORDER BY day", user_id)
