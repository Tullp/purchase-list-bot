
import React, {Component} from 'react';
import {formatDate} from "./utils";

export default class DateSelector extends Component {

    constructor(props) {
        super(props);
        this.inputFromDateRef = React.createRef();
        this.inputToDateRef = React.createRef();
        this.state = {fromDate: props.fromDate, toDate: props.toDate}
    }

    formatInputDateValue(date) {
        let month = (date.getMonth() + 1).toString().padStart(2, "0");
        let day = date.getDate().toString().padStart(2, "0");
        return `${date.getFullYear()}-${month}-${day}`;
    }

    showPicker(element) {
        if ("showPicker" in element) {
            element.showPicker();
        } else {
            element.focus();
        }
    }

    openFromDateInput = () => {
        this.showPicker(this.inputFromDateRef.current);
    }

    openToDateInput = () => {
        this.showPicker(this.inputToDateRef.current);
    }

    selectFromDate = (e) => {
        this.setState({fromDate: e.target.valueAsDate});
        this.props.onFromDateInput(e.target.valueAsDate);
    }

    selectToDate = (e) => {
        this.setState({toDate: e.target.valueAsDate});
        this.props.onToDateInput(e.target.valueAsDate);
    }

    render() {
        let fromDateString = formatDate(this.state.fromDate);
        let toDateString = formatDate(this.state.toDate);
        let fromDateInputValueString = this.formatInputDateValue(this.state.fromDate);
        let toDateInputValueString = this.formatInputDateValue(this.state.toDate);
        return (
            <div className="date-selector">
                <p>From</p>
                <button onClick={this.openFromDateInput}>{fromDateString}</button>
                <input value={fromDateInputValueString} type="date"
                       ref={this.inputFromDateRef} onInput={this.selectFromDate}/>
                <p>To</p>
                <button onClick={this.openToDateInput}>{toDateString}</button>
                <input value={toDateInputValueString} type="date"
                       ref={this.inputToDateRef} onInput={this.selectToDate}/>
            </div>
        );
    }
}
