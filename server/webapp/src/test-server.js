
import fetchMockLib from "fetch-mock";

let titles = ['Куриная грудка', 'Чашка', 'Яйца', 'Халат ', 'В биток', 'Сметана ', 'Рассыпной сыр ', 'Вино Cabernet ', 'Джин Gordons ', 'Пирожки с грибами 5шт ', 'Швепс 1.5л', 'Макароны', 'Вино Chardonnay', 'Вода 6шт', 'Джин Barrister ', 'Творог', 'Нектарин', 'Печенка куриная', 'Арбуз', 'Помидоры', 'Клубника', 'Молоко', 'Пирожки с грибами 4шт', 'Сахар', 'Хлеб ', 'Аренда квартиры', 'Хлопья', 'Хлеб', 'Пирожки с сыром 4шт', 'Сметана', 'Сыр', 'Расчёска', 'Пилочка для ногтей', 'Ножницы', 'Швепс 0.5л', 'Чипсы', 'Шампунь', 'Гель', 'Зарядка', 'Макаби', 'Донат', 'Масляной спрей', 'Лук', 'Сливки', 'Йогурт', 'Замороженные овощи', 'Мёд', 'Чай', 'Варенье', 'Кухонные весы', 'Мусорные пакеты', 'Чистилка для картошки', 'JetBrains', 'Рав-кав', 'Ноутбук', 'Соль', 'Лерые творог', 'Липтон', 'Лерыны сливки', 'Тнува', 'Телеграм Премиум', 'Что-то из магаза', 'Фарш куриный', 'Вода', 'Кола 0.3', 'Молотый перец', 'Бананы', 'Лимоны', 'Термос', 'Напиток', 'Кола', 'Домен', 'SSL сертификат', 'Молочный напиток', 'Замороженные овощи с мясом'];

let recommendations = [17.9, 15.5, 10.2, 21.9, 13, 29.3];

let history = [{id: 1, user_id: 123456, title: "Test purchase abcd", weight: 1, price: 12.9, timestamp: 1656768384}, {id: 2, user_id: 123456, title: "Test purchase 2 efdgsd", weight: 2.25, price: 2400, timestamp: 1656768384}, {id: 3, user_id: 123456, title: "Test purchase 3 ewdsed", weight: null, price: 3.5, timestamp: 1656768384}, {id: 4, user_id: 123456, title: "Test purchase 4 ewdswtered", weight: null, price: 3.5, timestamp: 1656768384}, {id: 5, user_id: 123456, title: "Test purchase 5 erovdm", weight: 2.25, price: 2400, timestamp: 1656768384}];

let grouped_purchases = [{title: "Test purchase abcd", weight: 1, count: null, price: 12.9}, {title: "Test purchase 2 efdgsd", weight: 2.25, count: null, price: 2400}, {title: "Test purchase 3 ewdsed", weight: null, count: 3, price: 3.5}, {title: "Test purchase 4 erovdm", weight: 2.25, count: null, price: 2400}, {title: "Test purchase 5 cdwefgfdv", weight: null, count: 3, price: 3.5}, {title: "Test purchase 6 abefjf ", weight: null, count: 3, price: 3.5}, {title: "Test purchase 7", weight: 2.25, count: null, price: 2400}, {title: "Test purchase 8", weight: null, count: 3, price: 3.5}];

let incomes = [{title: "Test income 1", income: 1200, timestamp: 1656768384}, {title: "Test income 2", income: 2555, timestamp: 1656788384}, {title: "Test income 3", income: 11270, timestamp: 1656798384}, {title: "Test income 4", income: 1200, timestamp: 1656768384}, {title: "Test income 5", income: 2555, timestamp: 1656788384}, {title: "Test income 6", income: 11270, timestamp: 1656798384}];

let fetchMock = fetchMockLib.sandbox();

fetchMock.post("/purchases/", () => ({items: grouped_purchases}));

fetchMock.post("/last/", () => ({items: history}));

fetchMock.post("/history/", () => ({items: history}));

fetchMock.post("/titles/", () => ({items: titles}));

fetchMock.post("/recommendations/", () => ({items: recommendations}));

fetchMock.post("/count-type/", () => ({count_type: null}));

fetchMock.post("/delete/", (url, opts) => {
    let payload = JSON.parse(opts.body);
    history = history.filter(p => !payload.purchase_ids.includes(p.id));
    return {ok: true}
});

fetchMock.post("/append/", (url, opts) => {
    let payload = JSON.parse(opts.body);
    if (payload.count == null) payload.count = 1;
    let max_id = Math.max(...history.map(p => p.id));
    for (let i = 0; i < parseInt(payload.count); i++) {
        history.push({
            id: max_id + i,
            user_id: payload.user_id,
            title: payload.title,
            price: parseFloat(payload.price),
            weight: parseFloat(payload.weight),
            timestamp: 9999
        });
    }
    return {ok: true}
});

fetchMock.post("/incomes/", () => ({items: incomes}));

export default fetchMock;