
import React, {Component} from 'react';

export default class Controls extends Component {

    render() {
        return (
            <div className="controls">
                {Object.keys(this.props).map(title =>
                    <button key={title} onClick={this.props[title]}>{title}</button>)}
            </div>
        );
    }
}
