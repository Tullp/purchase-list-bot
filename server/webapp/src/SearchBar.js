import React, {Component} from 'react';

export default class SearchBar extends Component {

    constructor(props) {
        super(props);
        this.state = {searchQuery: ""};
    }

    onInput = (e) => {
        this.setState({searchQuery: e.target.value}, () => {
            this.props.onInput(this.state.searchQuery)
        });
    }

    onSearchButtonClick = () => this.props.onSearchButtonClick(this.state.searchQuery);

    render() {
        return (
            <div className="search-bar">
                <input onInput={this.onInput} value={this.state.searchQuery}/>
                <button onClick={this.onSearchButtonClick}>Search</button>
            </div>
        );
    }
}
