
import {Component} from "react";

export default class Page extends Component {

    constructor(props) {
        super(props);
        this.state = {items: undefined, selected: [], lastSelected: null}
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.state.selected.length > 0 && JSON.stringify(this.state.items) !== JSON.stringify(prevState.items)) {
            this.setState({selected: []});
        }
    }

    componentDidMount() {
        let minHeight = window.Telegram.WebApp.viewportHeight - 100;
        document.getElementsByClassName("page")[0].style.setProperty("min-height", minHeight + "px");
        if (this.state.items == null) {
            this.loadItems();
        }
    }

    loadItems() { }

    getItems() {
        return this.state.items;
    }

    getSelected() {
        return this.getItems()?.filter((_, i) => this.state.selected.includes(i));
    }

    onSelect = (e, index) => {
        this.setState((state) => {
            if (e.shiftKey && state.lastClicked != null) {
                let range = [...Array(Math.abs(index - state.lastClicked) + 1).keys()]
                    .map(i => i + Math.min(index, state.lastClicked));
                if (state.action === "select") {
                    return {selected: [...state.selected, ...range]};
                } else if (state.action === "unselect") {
                    return {selected: state.selected.filter(val => !range.includes(val))};
                } else {
                    throw new Error("Unknown action state: " + state.action);
                }
            } else if (state.selected.includes(index)) {
                return {selected: state.selected.filter(val => val !== index), lastClicked: index, action: "unselect"};
            } else if (!state.selected.includes(index)) {
                return {selected: [...state.selected, index], lastClicked: index, action: "select"};
            } else {
                throw new Error("Unknown error");
            }
        });
    }
}

