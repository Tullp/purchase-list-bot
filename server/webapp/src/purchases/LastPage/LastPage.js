
import React from 'react';
import SearchBar from "../../SearchBar";
import PurchasesHistoryTable from "../PurchasesHistoryTable";
import Statistic from "./Statistic";
import PurchasesPage from "../PurchasesPage";
import Controls from "../../Controls";
import ToggleSwitch from "../../ToggleSwitch";
import GroupedPurchasesTable from "../GroupedPurchasesTable";

export default class LastPage extends PurchasesPage {

    constructor(props) {
        super(props);
        this.state.grouping = false;
    }

    loadItems() {
        this.props.apiService.get_last(this.state.grouping)
            .then(purchases => this.setState({items: purchases}));
    }

    onGroupingToggleClick = (grouping) => this.props.apiService.get_last(grouping)
        .then(purchases => this.setState({items: purchases, grouping: grouping}));

    table(items) {
        if (this.state.grouping) {
            return <GroupedPurchasesTable items={items} onSelect={this.onSelect} selected={this.state.selected}/>
        } else {
            return <PurchasesHistoryTable items={items} onSelect={this.onSelect} selected={this.state.selected}/>
        }
    }

    render() {
        let purchases = this.getItems();
        let selected = this.getSelected();

        return <div className="page">
            <div className="controls-container">
                <SearchBar onInput={this.onSearchQueryInput} onSearchButtonClick={this.onSearchButtonClick}/>
                <Controls Add={this.openAddWindow} Delete={this.openDeleteWindow}/>
            </div>
            <div className="controls-container">
                <Statistic items={selected?.length > 0 ? selected : purchases}/>
                <ToggleSwitch onClick={this.onGroupingToggleClick}/>
            </div>
            <div className="table-container">
                {this.table(purchases)}
            </div>
            {this.modalWindow()}
        </div>
    }
}
