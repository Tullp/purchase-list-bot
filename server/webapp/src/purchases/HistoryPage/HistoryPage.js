
import React from 'react';
import SearchBar from "../../SearchBar";
import PurchasesPage from "../PurchasesPage";
import Controls from "../../Controls";
import Statistic from "./Statistic";
import PurchasesHistoryTable from "../PurchasesHistoryTable";

export default class HistoryPage extends PurchasesPage {

    loadItems() {
        let fromDateTimestamp = this.state.fromDate.getTime() / 1000;
        let toDateTimestamp = this.state.toDate.getTime() / 1000;
        this.props.apiService.get_history(fromDateTimestamp, toDateTimestamp)
            .then(purchases => this.setState({items: purchases}));
    }

    render() {
        let purchases = this.getItems();
        let selected = this.getSelected();
        return <div className="page">
            <div className="controls-container">
                <SearchBar onInput={this.onSearchQueryInput} onSearchButtonClick={this.onSearchButtonClick}/>
                <Controls Add={this.openAddWindow} Delete={this.openDeleteWindow}/>
            </div>
            <div className="controls-container">
                <Statistic items={selected?.length > 0 ? selected : purchases}/>
                {this.dateSelector()}
            </div>
            <div className="table-container">
                <PurchasesHistoryTable items={purchases} onSelect={this.onSelect} selected={this.state.selected}/>
            </div>
            {this.modalWindow()}
        </div>
    }
}
