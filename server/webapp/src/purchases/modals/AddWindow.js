import React, {Component} from 'react';

export default class AddWindow extends Component {

    constructor(props) {
        super(props);
        this.state = {
            titles: null,
            prices: null,
            count_type: null,
            selected_amount_type: "count",
            amount: 1
        }
        props.apiService.get_titles()
            .then(titles => this.setState({titles}));
    }

    onTitleInput = (e) => {
        this.setState({title: e.target.value}, () => {
            this.props.apiService.get_recommendations(this.state.title)
                .then(prices => this.setState({prices}));
        });
    }

    onTitleBlur = () => this.props.apiService.get_count_type(this.state.title)
        .then(count_type => this.setState({count_type}));

    onPriceInput = (e) => this.setState({price: e.target.value});

    onAmountTypeInput = (e) => this.setState({selected_amount_type: e.target.value});

    onAmountInput = (e) => this.setState({amount: e.target.value});

    onAdd = () => {
        let data = {title: this.state.title, price: parseFloat(this.state.price)}
        if (this.state.count_type === "count") {
            data.count = parseInt(this.state.amount);
        } else if (this.state.count_type === "kg") {
            data.weight = parseFloat(this.state.amount);
        } else if (this.state.selected_amount_type === "count") {
            data.count = parseInt(this.state.amount);
        } else if (this.state.selected_amount_type === "kg") {
            data.weight = parseFloat(this.state.amount);
        }
        this.props.onAdd(data);
    }

    amountRow() {
        switch (this.state.count_type) {
            case "count": {
                return (<div className="input-row">
                    <p className="label count-label">Count:</p>
                    <input type="text" className="amount-input"
                           value={this.state.amount} onInput={this.onAmountInput}/>
                </div>);
            }
            case "kg": {
                return (<div className="input-row">
                    <p className="label kg-label">Kg: </p>
                    <input type="text" className="amount-input"
                           value={this.state.amount} onInput={this.onAmountInput}/>
                    <p className="kg-symbol">kg</p>
                </div>);
            }
            case null:
            default: {
                return (<div className="input-row">
                    <p className="label amount-label">Amount: </p>
                    <input type="text" className="amount-input"
                           value={this.state.amount} onInput={this.onAmountInput}/>
                    <select className="amount-type-input" onInput={this.onAmountTypeInput}>
                        <option value="count">Count</option>
                        <option value="kg">Kg</option>
                    </select>
                </div>);
            }
        }
    }

    render() {
        return (
            <div className="modal-container">
                <div className="modal add-window">
                    <div className="input-row">
                        <p className="label title-label">Title: </p>
                        <input type="text" className="text-input" list="text-input-hints"
                               onInput={this.onTitleInput} onBlur={this.onTitleBlur}/>
                        <datalist id="text-input-hints">
                            {this.state.titles?.map(title => <option value={title} key={title}></option>)}
                        </datalist>
                    </div>
                    <div className="input-row">
                        <p className="label price-label">Price: </p>
                        <input type="text" className="price-input"
                               list="price-input-hints" onInput={this.onPriceInput}/>
                        <p className="currency-symbol">₪</p>
                        <datalist id="price-input-hints">
                            {this.state.prices?.map(price => <option value={price} key={price}></option>)}
                        </datalist>
                    </div>
                    {this.amountRow()}
                    <div className="modal-controls">
                        <button onClick={this.props.onCancel}>Cancel</button>
                        <button onClick={this.onAdd}>Add</button>
                    </div>
                </div>
            </div>
        );
    }
}
