import React, {Component} from 'react';

export default class DeleteWindow extends Component {
    render() {
        return (
            <div className="modal-container">
                <div className="modal del-window">
                    <p>Do you want to delete {this.props.items.length} records?</p>
                    <div className="modal-controls">
                        <button onClick={this.props.onCancel}>Cancel</button>
                        <button onClick={() => this.props.onDelete(this.props.items.map(p => p.id))}>Delete</button>
                    </div>
                </div>
            </div>
        );
    }
}
