
import Page from "../Page";
import DeleteWindow from "./modals/DeleteWindow";
import AddWindow from "./modals/AddWindow";
import DateSelector from "../DateSelector";

export default class PurchasesPage extends Page {

    constructor(props) {
        super(props);
        this.state.toDate = new Date();
        this.state.fromDate = new Date(this.state.toDate.getTime()
            - (this.state.toDate.getDate() - 1) * 24 * 3600 * 1000
            - this.state.toDate.getHours() * 3600 * 1000);
    }

    getItems() {
        if (this.state.searchQuery != null) {
            return this.state.items.filter(item => item.title.includes(this.state.searchQuery));
        }
        return this.state.items;
    }

    modalWindow() {
        switch (this.state.modal) {
            case "delete": {
                return <DeleteWindow
                    items={this.getSelected()}
                    onCancel={() => this.setState({modal: null})}
                    onDelete={this.onDelete}/>
            }
            case "add": {
                return <AddWindow
                    apiService={this.props.apiService}
                    onCancel={() => this.setState({modal: null})}
                    onAdd={this.onAddPurchase}/>
            }
            default:
                return null;
        }
    }

    dateSelector() {
        return (<DateSelector
            onFromDateInput={this.onFromDateInput}
            onToDateInput={this.onToDateInput}
            fromDate={this.state.fromDate}
            toDate={this.state.toDate}
        />);
    }

    onFromDateInput = (date) => {
        this.setState({fromDate: date}, () => this.loadItems());
    }
    onToDateInput = (date) => {
        this.setState({toDate: date}, () => this.loadItems());
    }

    onSearchButtonClick = (searchQuery) => this.setState((state) => {
        let prevItems = this.state.items.filter((p) => p.title.includes(state.searchQuery));
        let items = this.state.items.filter((p) => p.title.includes(searchQuery));
        return {searchQuery: searchQuery, selected: prevItems.length !== items.length ? [] : state.selected};
    });
    onSearchQueryInput = (searchQuery) => this.onSearchButtonClick(searchQuery);

    openDeleteWindow = () => this.setState({modal: "delete"});
    onDelete = (id_list) => {
        this.props.apiService.delete(id_list)
            .then(ok => {
                this.setState({modal: null});
                if (ok) this.loadItems();
            });
    }

    openAddWindow = () => this.setState({modal: "add"});
    onAddPurchase = (data) => {
        this.props.apiService.add(data)
            .then(ok => {
                this.setState({modal: null});
                if (ok) this.loadItems();
            })
    }
}
