
import React from 'react';
import Statistic from "./Statistic";
import SearchBar from "../../SearchBar";
import Controls from "../../Controls";
import PurchasesPage from "../PurchasesPage";
import GroupedPurchasesTable from "../GroupedPurchasesTable";

export default class GroupedPurchasesPage extends PurchasesPage {

    loadItems() {
        let fromDateTimestamp = this.state.fromDate.getTime() / 1000;
        let toDateTimestamp = this.state.toDate.getTime() / 1000;
        this.props.apiService.get_purchases(fromDateTimestamp, toDateTimestamp)
            .then(purchases => this.setState({items: purchases}));
    }

    render() {
        let purchases = this.getItems();
        let selected = this.getSelected();
        return <div className="page">
            <div className="controls-container">
                <SearchBar onInput={this.onSearchQueryInput} onSearchButtonClick={this.onSearchButtonClick}/>
                <Controls Add={this.openAddWindow}/>
            </div>
            <div className="controls-container">
                <Statistic items={selected?.length > 0 ? selected : purchases}/>
                {this.dateSelector()}
            </div>
            <div className="table-container">
                <GroupedPurchasesTable items={purchases} onSelect={this.onSelect} selected={this.state.selected}/>
            </div>
            {this.modalWindow()}
        </div>
    }
}
