
import React, {Component} from 'react';

export default class Statistic extends Component {
    render() {
        if (this.props.items == null) {
            return null;
        }
        let total = this.props.items
            .map(p => p.price * (p.weight == null ? p.count : p.weight))
            .reduce((sum, price) => sum + price, 0)
            .toFixed(1);
        return (
            <div className="statistic">
                <p>Total cost: {total}₪</p>
            </div>
        );
    }
}
