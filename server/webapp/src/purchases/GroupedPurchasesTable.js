import React, {Component} from 'react';

export default class GroupedPurchasesTable extends Component {

    render() {
        if (this.props.items === undefined) {
            return <p>Loading...</p>
        }
        if (this.props.items === null) {
            return <p>Error</p>
        }
        return (
            <table className="page-table grouped">
                <tbody>
                {this.props.items.map((purchase, i) => {
                    let amount = purchase.weight === null ? purchase.count : purchase.weight;
                    let amountString = purchase.weight === null ? "x" + amount : amount + "кг";
                    let onClick = (e) => this.props.onSelect(e, i);
                    let className = this.props.selected?.includes(i) ? "selected" : "";
                    return (<tr onClick={onClick} className={className} key={i}>
                        <td className="title">{purchase.title}</td>
                        <td className="price">{purchase.price.toFixed(1)}₪</td>
                        <td className="amount">{amountString}</td>
                        <td className="total-price">{(purchase.price * amount).toFixed(0)}₪</td>
                    </tr>)
                })}
                </tbody>
            </table>
        );
    }
}
