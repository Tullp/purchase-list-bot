
import React, {Component} from 'react';
import {formatDate} from "../utils";

export default class PurchasesHistoryTable extends Component {

    render() {
        if (this.props.items === undefined) {
            return <p>Loading...</p>
        }
        if (this.props.items === null) {
            return <p>Error</p>
        }
        return (
            <table className="page-table history">
                <tbody>
                {this.props.items.map((purchase, i) => {
                    let onClick = (e) => this.props.onSelect(e, i);
                    let className = this.props.selected?.includes(i) ? "selected" : "";
                    let total = purchase.weight == null ? purchase.price : purchase.price * purchase.weight;
                    return (<tr onClick={onClick} className={className} key={i}>
                        <td className="title">{purchase.title}</td>
                        <td className="price">{purchase.price.toFixed(1)}₪</td>
                        <td className="weight">{purchase.weight != null ? purchase.weight.toFixed(1) + 'кг' : 'x1'}</td>
                        <td className="total-price">{total.toFixed(0)}₪</td>
                        <td className="time">{formatDate(new Date(purchase.timestamp * 1000))}</td>
                    </tr>)
                })}
                </tbody>
            </table>
        );
    }
}
