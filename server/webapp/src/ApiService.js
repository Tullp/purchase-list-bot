import {getUser} from "./utils";

export default class ApiService {

    constructor(fetch) {
        this.fetch = fetch;
    }

    delete(purchase_ids) {
        return this.__post("/delete/", {purchase_ids})
            .then(resp => resp?.ok)
    }

    get_purchases(from_timestamp = null, to_timestamp = null) {
        return this.__post("/purchases/", {from_timestamp, to_timestamp})
            .then(resp => resp?.items);
    }

    get_history(from_timestamp = null, to_timestamp = null) {
        return this.__post("/history/", {from_timestamp, to_timestamp})
            .then(resp => resp?.items)
    }

    get_last(grouping) {
        return this.__post("/last/", {grouping})
            .then(resp => resp?.items)
    }

    get_titles() {
        return this.__post("/titles/")
            .then(resp => resp?.items)
    }

    get_recommendations(title) {
        return this.__post("/recommendations/", {title})
            .then(resp => resp?.items)
    }

    get_count_type(title) {
        return this.__post("/count-type/", {title})
            .then(resp => resp?.count_type)
    }

    add(data) {
        return this.__post("/append/", data)
            .then(resp => resp?.ok)
    }

    get_incomes() {
        return this.__post("/incomes/")
            .then(resp => resp.items);
    }

    __post(url, data) {
        if (data == null) data = {}
        return this.__request(url, "POST", data);
    }

    __request(url, method, data) {
        data.user_id = getUser().id;
        let params = {
            method: method,
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(data)
        }
        return this.fetch(url, params)
            .then(resp => resp.json())
            .catch(() => null);
    }
}
