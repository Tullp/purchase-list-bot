
let themes = {
    "#17212b": {
        firstLevelBackground: "#17212b",
        secondLevelBackground: "#0e1621",
        thirdLevelBackground: "#283441",
        controlColor: "#5288c1",
        selectionColor: "#2e70a5",
        inputTextColor: "#adb2b7",
    },
    "#1c1c1d": {
        firstLevelBackground: "#0e0e0e",
        secondLevelBackground: "#1c1c1d",
        thirdLevelBackground: "#283441",
        controlColor: "#399afb",
        selectionColor: "#2d79f7",
        inputTextColor: "#9f9ea3",
    }
}

export default themes;
