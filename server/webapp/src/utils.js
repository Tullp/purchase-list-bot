
export function getUser() {
    return window.Telegram.WebApp.initDataUnsafe.user || {
        "id": 165647468,
        "first_name": "Maks",
        "last_name": "",
        "username": "OwnerOfForest",
        "language_code": "en"
    };
}

export function formatDate(date) {
    // let month = (date.getMonth() + 1).toString().padStart(2, "0");
    let month = date.toLocaleString("en-us", {month: "short"});
    let day = date.getDate();
    return `${month} ${day}`;
}
