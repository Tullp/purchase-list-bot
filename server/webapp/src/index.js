
import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import ApiService from "./ApiService";
import themes from "./themes";
import fetchMock from "./test-server";

let fetch = window.location.href.startsWith("http://localhost") ?
    fetchMock : (url, params) => window.fetch(url, params);

const root = document.getElementById('root');

let theme = themes[window.Telegram.WebApp.backgroundColor || "#17212b"];
let html = document.getElementsByTagName("html")[0];
html.style.setProperty("--first-level-background", theme.firstLevelBackground);
html.style.setProperty("--second-level-background", theme.secondLevelBackground);
html.style.setProperty("--third-level-background", theme.thirdLevelBackground);
html.style.setProperty("--control-color", theme.controlColor);
html.style.setProperty("--selection-color", theme.selectionColor);
html.style.setProperty("--input-text-color", theme.inputTextColor);

ReactDOM.createRoot(root).render(<App apiService={new ApiService(fetch)} />);
