
import React, {Component} from 'react';

export default class ToggleSwitch extends Component {

    constructor(props) {
        super(props);
        this.state = {checked: false}
    }

    onClick = () => this.setState((state) => ({checked: !state.checked}), () => {
        this.props.onClick(this.state.checked);
    });

    render() {
        return (
            <div className="controls-switch">
                <p>Grouping</p>
                <label className="switch">
                    <input type="checkbox" onClick={this.onClick} checked={this.state.checked}/>
                    <span className="slider"></span>
                </label>
            </div>
        );
    }
}
