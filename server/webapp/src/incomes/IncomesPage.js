
import Page from "../Page";
import IncomesTable from "./IncomesTable";
import Statistic from "./Statistic";

export default class IncomesPage extends Page {

    loadItems() {
        this.props.apiService.get_incomes()
            .then(incomes => this.setState({items: incomes}));
    }

    render() {
        let incomes = this.getItems();
        let selected = this.getSelected();
        return <div className="page">
            <div className="controls-container">
                <Statistic items={selected?.length > 0 ? selected : incomes}/>
            </div>
            <div className="table-container">
                <IncomesTable items={incomes} onSelect={this.onSelect} selected={this.state.selected}/>
            </div>
        </div>
    }
}
