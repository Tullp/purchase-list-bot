
import React, {Component} from 'react';
import {formatDate} from "../utils";

export default class IncomesTable extends Component {

    render() {
        if (this.props.items === undefined) {
            return <p>Loading...</p>
        }
        if (this.props.items === null) {
            return <p>Error</p>
        }
        return (
            <table className="page-table incomes">
                <tbody>
                {this.props.items.map((incomeInfo, i) => {
                    let onClick = (e) => this.props.onSelect(e, i);
                    let className = this.props.selected?.includes(i) ? "selected" : "";
                    return (<tr onClick={onClick} className={className} key={i}>
                        <td className="title">{incomeInfo.title}</td>
                        <td className="income">{incomeInfo.income.toFixed(0)}₪</td>
                        <td className="time">{formatDate(new Date(incomeInfo.timestamp * 1000))}</td>
                    </tr>)
                })}
                </tbody>
            </table>
        );
    }
}
