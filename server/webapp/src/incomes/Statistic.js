
import React, {Component} from 'react';

export default class Statistic extends Component {
    render() {
        if (this.props.items == null) {
            return null;
        }
        let total = this.props.items
            .map(info => info.income)
            .reduce((sum, income) => sum + income, 0)
            .toFixed(0);
        return (
            <div className="statistic">
                <p>Total income: {total}₪</p>
            </div>
        );
    }
}
