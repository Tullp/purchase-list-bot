import React from 'react';
import GroupedPurchasesPage from "./purchases/GroupedPurchasesPage/GroupedPurchasesPage";
import HistoryPage from "./purchases/HistoryPage/HistoryPage";
import LastPage from "./purchases/LastPage/LastPage";
import IncomesPage from "./incomes/IncomesPage";

export class App extends React.Component {

    constructor(props) {
        super(props);
        this.tabs = {
            "Purchases": <GroupedPurchasesPage apiService={props.apiService}/>,
            "History": <HistoryPage apiService={props.apiService}/>,
            "Last": <LastPage apiService={props.apiService}/>,
            "Incomes": <IncomesPage apiService={props.apiService}/>
        };
        this.state = {selected: "Last"};
    }

    handleClick(e) {
        if (this.state.selected !== e.target.innerText) {
            this.setState({selected: e.target.innerText});
        }
    }

    render() {
        return <div className="app">
            <div className="menu">
                {Object.keys(this.tabs).map(tabName => (
                    <div key={tabName} onClick={(e) => this.handleClick(e)}
                         className={this.state.selected === tabName ? "menu-tab selected" : "menu-tab"}>
                        <button>{tabName}</button>
                    </div>
                ))}
            </div>
            {this.tabs[this.state.selected]}
        </div>
    }
}

export default App;
