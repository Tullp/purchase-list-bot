
from datetime import datetime

from flask_restful import Resource

from server.DependencyManager import DependencyManager


class WebAppResource(Resource):

	path = None

	def __init__(self, dependency_manager: DependencyManager):
		self.dependency_manager = dependency_manager
