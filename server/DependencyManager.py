from flask import Flask

from services.income.IncomeService import IncomeService
from services.purchase.PurchaseService import PurchaseService


class DependencyManager:

	def __init__(self, app: Flask, purchases_service: PurchaseService, incomes_service: IncomeService):
		self.app = app
		self.purchases_service = purchases_service
		self.incomes_service = incomes_service
