
import platform
import threading
import traceback

from BotCore import DataBase
from flask import Flask
from flask_restful import Api

from server.resources import *
from server.DependencyManager import DependencyManager
from server.ResourceBinder import ResourceBinder
from services.income.IncomeDao import IncomeDao
from services.income.IncomeService import IncomeService
from services.purchase.PurchaseDao import PurchaseDao
from services.purchase.PurchaseService import PurchaseService


class Server:

	def __init__(self, database: DataBase):

		self.app = Flask(__name__, static_url_path='', static_folder='webapp/build')
		api = Api(self.app)

		database = database
		purchases_service = PurchaseService(PurchaseDao(database))
		incomes_service = IncomeService(IncomeDao(database), purchases_service)
		dependency_manager = DependencyManager(self.app, purchases_service, incomes_service)

		for resource in ResourceBinder.resources:
			api.add_resource(resource, resource.path, resource_class_args=(dependency_manager,))
		self.app.register_error_handler(Exception, self.error_handler)

	def start(self):
		if platform.system() == "Windows":
			threading.Thread(target=self.app.run, name="server").start()
		else:
			threading.Thread(target=self.app.run, name="server", kwargs={
				"host": "ownerofforest.com",
				"port": 5555,
				"ssl_context": ("/root/ssl/cert.pem", "/root/ssl/key.pem")
			}).start()

	@staticmethod
	def error_handler(e):
		print(traceback.format_exc())
		return e
