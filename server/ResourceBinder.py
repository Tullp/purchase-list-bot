from server.WebAppResource import WebAppResource


class ResourceBinder:

	resources: list[WebAppResource] = []

	@classmethod
	def bind(cls, clazz):
		cls.resources.append(clazz)
		return clazz
