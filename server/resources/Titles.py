
from flask import request

from server.ResourceBinder import ResourceBinder
from server.WebAppResource import WebAppResource


@ResourceBinder.bind
class Titles(WebAppResource):

	path = "/titles/"

	def post(self):
		payload = request.get_json()
		titles = self.dependency_manager.purchases_service.get_titles(payload["user_id"])
		return {"items": titles}

