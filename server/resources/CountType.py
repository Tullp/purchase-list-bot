
from flask import request

from server.ResourceBinder import ResourceBinder
from server.WebAppResource import WebAppResource


@ResourceBinder.bind
class CountType(WebAppResource):

	path = "/count-type/"

	def post(self):
		payload = request.get_json()
		is_countable = self.dependency_manager.purchases_service.is_item_countable(payload["user_id"], payload.get("title"))
		if is_countable is None:
			return {"count_type": None}
		return {"count_type": "count" if is_countable else "kg"}

