
from flask import request

from server.ResourceBinder import ResourceBinder
from server.WebAppResource import WebAppResource


@ResourceBinder.bind
class Last(WebAppResource):

	path = "/last/"

	def post(self):
		payload = request.get_json()
		grouping = payload["grouping"]
		last_purchases = self.dependency_manager.purchases_service.get_last_purchases(payload["user_id"], grouping)
		if grouping:
			return {
				"items": [{
					"title": group.title,
					"price": group.price,
					"weight": group.weight,
					"count": group.count
				} for group in last_purchases]
			}
		else:
			return {
				"items": [{
					"id": purchase.id,
					"user_id": purchase.user_id,
					"title": purchase.title,
					"price": purchase.price,
					"weight": purchase.weight,
					"timestamp": purchase.timestamp
				} for purchase in last_purchases]
			}

