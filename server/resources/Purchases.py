from datetime import datetime

from flask import request

from server.ResourceBinder import ResourceBinder
from server.WebAppResource import WebAppResource


@ResourceBinder.bind
class Purchases(WebAppResource):

	path = "/purchases/"

	@staticmethod
	def __get_timestamps(payload):
		month_start = int(datetime.now().replace(day=1, hour=0, minute=0).timestamp())
		now = int(datetime.now().timestamp())
		from_timestamp = payload.get("from_timestamp", 0) or month_start
		to_timestamp = payload.get("to_timestamp", 0) or now
		return from_timestamp, to_timestamp

	def post(self):
		payload = request.get_json()
		from_timestamp, to_timestamp = self.__get_timestamps(payload)
		purchases = self.dependency_manager.purchases_service.get_purchases(
			payload["user_id"], from_timestamp, to_timestamp)
		grouped_purchases = self.dependency_manager.purchases_service.group_purchases_by_title_and_price(purchases)
		return {
			"items": [{
				"title": group.title,
				"price": group.price,
				"weight": group.weight,
				"count": group.count
			} for group in grouped_purchases]
		}

