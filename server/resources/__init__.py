
import os

__all__ = [
	file.split(".")[0] for file in
	os.listdir(os.path.join("server", "resources"))
	if file != "__init__.py"
]
