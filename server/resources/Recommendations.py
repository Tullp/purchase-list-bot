
from flask import request

from server.ResourceBinder import ResourceBinder
from server.WebAppResource import WebAppResource


@ResourceBinder.bind
class Recommendations(WebAppResource):

	path = "/recommendations/"

	def post(self):
		payload = request.get_json()
		recommendations = self.dependency_manager.purchases_service.get_price_recommendations(
			payload["user_id"], payload.get("title"))
		return {"items": recommendations}

