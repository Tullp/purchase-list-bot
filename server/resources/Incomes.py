
from flask import request

from server.ResourceBinder import ResourceBinder
from server.WebAppResource import WebAppResource


@ResourceBinder.bind
class Incomes(WebAppResource):

	path = "/incomes/"

	def post(self):
		payload = request.get_json()
		incomes = self.dependency_manager.incomes_service.get_user_incomes(payload["user_id"])
		incomes.sort(key=lambda info: info.timestamp, reverse=True)
		return {"items": [{
			"id": info.id,
			"title": info.title or "No title",
			"income": info.income,
			"timestamp": info.timestamp
		} for info in incomes]}

