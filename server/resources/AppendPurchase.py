
from flask import request

from server.ResourceBinder import ResourceBinder
from server.WebAppResource import WebAppResource
from services.purchase.PurchaseInfo import PurchaseInfo


@ResourceBinder.bind
class AppendPurchase(WebAppResource):

	path = "/append/"

	def post(self):
		payload = request.get_json()
		if not self.dependency_manager.purchases_service.check_count_type_matching(payload["user_id"], PurchaseInfo(payload)):
			return {"ok": False, "desc": "Invalid count type."}
		for i in range(payload.get("count", 1)):
			self.dependency_manager.purchases_service.create(**payload)
		return {"ok": True}

