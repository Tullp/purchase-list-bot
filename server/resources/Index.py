from flask import send_from_directory

from server.ResourceBinder import ResourceBinder
from server.WebAppResource import WebAppResource


@ResourceBinder.bind
class Index(WebAppResource):

	path = "/"

	def get(self):
		return send_from_directory(self.dependency_manager.app.static_folder, 'index.html')

