from datetime import datetime

from flask import request

from server.ResourceBinder import ResourceBinder
from server.WebAppResource import WebAppResource


@ResourceBinder.bind
class History(WebAppResource):

	path = "/history/"

	@staticmethod
	def __get_timestamps(payload):
		month_start = int(datetime.now().replace(day=1, hour=0, minute=0).timestamp())
		now = int(datetime.now().timestamp())
		from_timestamp = payload.get("from_timestamp", 0) or month_start
		to_timestamp = payload.get("to_timestamp", 0) or now
		return from_timestamp, to_timestamp

	def post(self):
		payload = request.get_json()
		from_timestamp, to_timestamp = self.__get_timestamps(payload)
		purchases = self.dependency_manager.purchases_service.get_purchases(payload["user_id"], from_timestamp, to_timestamp)
		return {
			"items": [{
				"id": purchase.id,
				"user_id": purchase.user_id,
				"title": purchase.title,
				"price": purchase.price,
				"weight": purchase.weight,
				"timestamp": purchase.timestamp
			} for purchase in purchases]
		}
