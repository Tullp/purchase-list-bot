
from flask import request

from server.ResourceBinder import ResourceBinder
from server.WebAppResource import WebAppResource


@ResourceBinder.bind
class Delete(WebAppResource):

	path = "/delete/"

	def post(self):
		payload = request.get_json()
		purchases = payload["purchase_ids"]
		for p_id in purchases:
			self.dependency_manager.purchases_service.delete(p_id)
		return {"ok": True}

